﻿$locations=Get-AzureLocation | select Name
$region=Get-Random -input $locations.Name
$rgname = "VMinCode"
$storagename = "tptomavmdiskstore"
New-AzurermResourceGroup -Name $rgname -location $region
New-AzurermStorageAccount -ResourceGroupName $rgname –StorageAccountName $storagename –Location $region -Type Standard_LRS
$vmDNSname = "tptoma-vmincode"
$subnetName = "Subnet-1"
$subnet = New-AzureRmVirtualNetworkSubnetConfig -Name $subnetName -AddressPrefix "10.0.64.0/24"
$vnet = New-AzureRmVirtualNetwork -Name "VNET" -ResourceGroupName $rgname -Location $region -AddressPrefix "10.0.0.0/16" -Subnet $subnet
$subnet = Get-AzureRmVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork $vnet
$pip = New-AzureRmPublicIpAddress -ResourceGroupName $rgname -Name "vip1" -Location $region -AllocationMethod Dynamic -DomainNameLabel $vmDNSname
$nic = New-AzureRmNetworkInterface -ResourceGroupName $rgname -Name "nic1" -Subnet $subnet -Location $region -PublicIpAddress $pip -PrivateIpAddress "10.0.64.4"



$publisher = "MicrosoftWindowsServer"
$offer = "WindowsServer"
$sku = "2012-R2-Datacenter"
$version = "latest"
$cred = Get-Credential


$vmConfig = New-AzureRmVMConfig -VMName $vmDNSname -VMSize "Standard_A1" 
Set-AzureRmVMOperatingSystem -VM $vmconfig -Windows -ComputerName "contoso-w1" -Credential $cred -ProvisionVMAgent -EnableAutoUpdate 
$vmConfig | Set-AzureRmVMSourceImage -PublisherName $publisher -Offer $offer -Skus $sku -Version $version
$vmConfig | Set-AzureRmVMOSDisk -Name "$vmDNSname-w1" -VhdUri "https://$storagename.blob.core.windows.net/vhds/$vmDNSname-w1-os.vhd" -Caching ReadWrite -CreateOption fromImage
$vmConfig | add-AzureRmVMNetworkInterface -id $nic.id
New-AzureRmVM -ResourceGroupName $rgname -Location $region -VM $vmConfig

